//// trial comment ////

import {spawnGltfX, spawnEntity, spawnBoxX, spawnPlaneX, spawnTextX} from './modules/SpawnerFunctions'

import {BuilderHUD} from './modules/BuilderHUD'
import utils from "../node_modules/decentraland-ecs-utils/index"


const scene = new Entity()
const transform = new Transform({
  position: new Vector3(0, 0, 0),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
scene.addComponentOrReplace(transform)
engine.addEntity(scene)

const floorBaseGrass_01 = new Entity()
floorBaseGrass_01.setParent(scene)
const gltfShape = new GLTFShape('models/FloorBaseGrass_01/FloorBaseGrass_01.glb')
floorBaseGrass_01.addComponentOrReplace(gltfShape)
const transform_2 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_01.addComponentOrReplace(transform_2)
engine.addEntity(floorBaseGrass_01)


const cakeShape = new GLTFShape('models/cake/cake.glb')
const cake = spawnGltfX(cakeShape, 8,2,5,  -90,90,0,  2,3,2)

//Define the positions of the path
let path = []
path[0] = new Vector3(1, 1, 1)
path[1] = new Vector3(1, 8, 15)
path[2] = new Vector3(15, 1, 12)
path[3] = new Vector3(15, 3, 1)

// Scale Entity
let StartSize = new Vector3(0.01, 0.01, 0.0)
let EndSize = new Vector3(7, 7, 7)

// Move entity

cake.addComponent(new utils.FollowPathComponent(path, 30))
cake.addComponent(new utils.ScaleTransformComponent(StartSize, EndSize, 40))




/*
//Define start and end positions
let path = []
path[0] = new Vector3(1, 1, 1)
path[1] = new Vector3(1, 1, 15)
path[2] = new Vector3(15, 1, 15)
path[3] = new Vector3(15, 1, 1)

// Move entity
cake.addComponent(new utils.FollowPathComponent(path[0], 5))


const cakeText = spawnTextX('Strange Cake', 8,3,8,   0,0,0,   1,1,1)
*/

const hud:BuilderHUD =  new BuilderHUD()
hud.setDefaultParent(scene)
hud.attachToEntity(cake)

/*
0,0:  --------------- BuilderHUD entities -------------
preview.js:8 0,0:  Existing(8,2,5,  -90,90,0,  2,3,2)

preview.js:8 0,0:  -------------------------------------------------



// Create entity

// Create entity
const box = new Entity()

// Give entity a shape and transform
box.addComponent(new BoxShape())
box.addComponent(new Transform())
*/
